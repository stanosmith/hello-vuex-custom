# hello-vuex-custom

> A Vue.js project based on tutorials by [RAS Web Design](https://www.youtube.com/channel/UCaVKqPN7n34QciaxdwD_dsw)

* Vuex tutorial video: https://www.youtube.com/watch?v=ARtSqjel6Ho
* Vuex + Firebase tutorial video: https://www.youtube.com/watch?v=niPgyv53x8c
* Firebase project: https://console.firebase.google.com/project/vuex-firebase-tutorial/overview


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

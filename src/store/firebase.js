import firebase from 'firebase'

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyAM0-ySvro3eEcPev-NpkXheLZlIFaC_2s',
  authDomain: 'vuex-firebase-tutorial.firebaseapp.com',
  databaseURL: 'https://vuex-firebase-tutorial.firebaseio.com',
  projectId: 'vuex-firebase-tutorial',
  storageBucket: 'vuex-firebase-tutorial.appspot.com',
  messagingSenderId: '199618981065'
}
firebase.initializeApp(config)

export default {
  database: firebase.database()
}

import Vue from 'vue'
import Vuex from 'vuex'
import firebase from './firebase'

Vue.use(Vuex)
Vue.use(firebase)

export const store = new Vuex.Store({
  strict: true,
  state: {
    showLogo: true
  },
  getters: {
    showLogo: state => {
      return state.showLogo
    }
  },
  mutations: {
    toggleLogo: (state, toggledState) => {
      state.showLogo = toggledState
    }
  },
  actions: {
    toggleLogo: context => {
      firebase.database.ref('settings/setting').set(!context.state.showLogo)
    },
    // This is only called once, but the listener will continue to listen to changes to the value in Firebase
    getFirebaseDB: context => {
      firebase.database.ref('settings/setting').on('value', snapshot => {
        context.commit('toggleLogo', snapshot.val())
      })
    }
  }
})
